﻿#include "functions.h"
#include <conio.h>

using std::cout;
using std::endl;

inline void table(List<Attribute> &list, FilterAttribute &aFilter, List<Attribute*> &resFilter) {
    int value = find(list, aFilter, resFilter);
    cout << "+---------------------------------------------+" << endl
        << "| Найдено: " << value << std::left << std::setw(34 - count(value)) << " записей по данному фильтру" << " |" << endl
        << "+---------------------------------------------+" << endl
        << std::left << std::setw(21) << "| Код: " << std::left << std::setw(24) << ((!aFilter.bCode) ? "N/a" : std::to_string(aFilter.bCode))
        + " - " + ((!aFilter.eCode) ? "N/a" : std::to_string(aFilter.eCode)) << " |" << endl
        << std::left << std::setw(21) << "| Название: " << std::left << std::setw(24) << ((aFilter.name.empty()) ? "N/a" : aFilter.name) << " |" << endl
        << std::left << std::setw(21) << "| Степень важности: " << std::left << std::setw(24) << ((aFilter.importance.empty()) ? "N/a" : aFilter.importance) << " |" << endl
        << std::left << std::setw(21) << "| Еденица измерения: " << std::left << std::setw(24) << ((aFilter.unit.empty()) ? "N/a" : aFilter.unit) << " |" << endl
        << "+---------------------------------------------+" << endl;
}

inline void table(List<Company> &list, FilterCompany &cFilter, List<Company*> &resFilter) {
    int value = find(list, cFilter, resFilter);
    cout << "+-----------------------------------------------+" << endl
        << "| Найдено: " << value << std::left << std::setw(36 - count(value)) << " записей по заданному фильтру" << " |" << endl
        << "+-----------------------------------------------+" << endl
        << "| " << std::left << std::setw(25) << "Код: " << std::setw(20) << ((!cFilter.bCode) ? "N/a" : std::to_string(cFilter.bCode))
        + " - " + ((!cFilter.eCode) ? "N/a" : std::to_string(cFilter.eCode)) << " |" << endl
        << "| " << std::left << std::setw(25) << "Название: " << std::setw(20) << (cFilter.name.empty() ? "N/a" : cFilter.name) << " |" << endl
        << "| " << std::left << std::setw(25) << "Банковские риквизиты: " << std::setw(20) << (cFilter.bank_details.empty() ? "N/a" : cFilter.bank_details) << " |" << endl
        << "| " << std::left << std::setw(25) << "Контактное лицо: " << std::setw(20) << (cFilter.contact_person.empty() ? "N/a" : cFilter.contact_person) << " |" << endl
        << "| " << std::left << std::setw(25) << "Контактный телефон: " << std::setw(20) << (cFilter.telephone.empty() ? "N/a" : cFilter.telephone) << " |" << endl
        << "+-----------------------------------------------+" << endl;
}

inline void table(List<AttributeValue> &list, FilterValue &vFilter, List<AttributeValue*> &resFilter) {
    int value = find(list, vFilter, resFilter);
    cout << "+------------------------------------------------------+" << endl
        << "| " << "Найдено: " << value << std::left << std::setw(43 - count(value)) << " записей по заданному фильтру" << " |" << endl
        << "+------------------------------------------------------+" << endl
        << std::left << std::setw(34) << "| Код показателя: " << std::left << std::setw(20)
        << ((!vFilter.attribute.bCode) ? ("N/a") : (std::to_string(vFilter.attribute.bCode)))
        + " - " + ((!vFilter.attribute.eCode) ? ("N/a") : (std::to_string(vFilter.attribute.eCode))) << " |" << endl
        << std::left << std::setw(34) << "| Название показателя: " << std::left << std::setw(20)
        << (vFilter.attribute.name.empty() ? "N/a" : vFilter.attribute.name) << " |" << endl
        << std::left << std::setw(34) << "| Степень важности показателя: "
        << (vFilter.attribute.importance.empty() ? "N/a" : vFilter.attribute.importance) << " |" << endl
        << std::left << std::setw(34) << "| Еденица измерения: " << std::left << std::setw(20)
        << (vFilter.attribute.unit.empty() ? "N/a" : vFilter.attribute.unit) << " |" << endl
        << std::left << std::setw(34) << "| Код компании: " << std::left << std::setw(20)
        << ((!vFilter.company.bCode) ? ("N/a") : (std::to_string(vFilter.company.bCode)))
        + " - " + ((!vFilter.company.eCode) ? ("N/a") : (std::to_string(vFilter.company.eCode))) << " |" << endl
        << std::left << std::setw(34) << "| Название компании: " << std::left << std::setw(20)
        << (vFilter.company.name.empty() ? "N/a" : vFilter.company.name) << " |" << endl
        << std::left << std::setw(34) << "| Значение показателя: " << std::left << std::setw(20)
        << ((!vFilter.bValue) ? ("N/a") : (std::to_string(vFilter.bValue)))
        + " - " + ((!vFilter.eValue) ? ("N/a") : (std::to_string(vFilter.eValue))) << " |" << endl
        << std::left << std::setw(34) << "| Дата: " << std::left << std::setw(20)
        << (!vFilter.bDate ? "N/a" : (std::to_string(vFilter.bDate.day) + ":" +
            std::to_string(vFilter.bDate.month) + ":" + std::to_string(vFilter.bDate.year)))
        + " - " + (!vFilter.eDate ? "N/a" : (std::to_string(vFilter.eDate.day) + ":" +
            std::to_string(vFilter.eDate.month) + ":" + std::to_string(vFilter.eDate.year))) << " |" << endl
        << "+----------------------------------------------------+" << endl;
}

void enter_data(List<Attribute> &listAttribute,
    List<Company> &listCompany,
    List<AttributeValue> &listAttributeValue) {
    bool flag = true;
    int menu;
    do {
        menu = data_menu();
        system("cls");
        switch (menu) {
        case '1':
            enter_attribute(listAttribute);
            system("pause");
            system("cls");
            break;
        case '2':
            enter_company(listCompany);
            system("pause");
            system("cls");
            break;
        case '3':
            enter_attribute_value(listAttribute, listCompany, listAttributeValue);
            system("pause");
            system("cls");
            break;
        case 27:
        case '0':
            flag = false;
            break;
        default:
            cout << "Неверный ввод: " << menu << endl;
            break;
        }
    } while (flag);
}
// Сохранение в файл списка Attribute
bool save_in_file(List<Attribute> &listAttribute) {
    std::ofstream fout("save/attribute.txt", std::ios_base::out);
    bool result = true;
    if (fout.is_open()) {
        for (auto &i : listAttribute) {
            fout << endl << i.code << endl << i.name << endl
                << i.importance << endl << i.unit;
        }
    }
    else {
        cout << "Ошибка создания/открытия файла attribute.txt" << endl;
        result = false;
    }
    return result;
}
// Сохранение в файл списка Company
bool save_in_file(List<Company> &listCompany) {
    std::ofstream fout("save/companies.txt", std::ios_base::out);
    bool result = true;
    if (fout.is_open()) {
        for (auto &i : listCompany) {
            fout << endl <<i.code << endl << i.name << endl
                << i.bank_details << endl << i.contact_person << endl << i.telephone;
        }
    }
    else {
        cout << "Ошибка создания/открытия файла companies.txt" << endl;
        result = false;
    }
    return result;
}
// Сохранение в файл списка AttributeValue
bool save_in_file(List<AttributeValue> &listAttributeValue) {
    std::ofstream fout("save/dynamics.txt", std::ios_base::out);
    bool result = true;
    if (fout.is_open()) {
        for (auto &i : listAttributeValue) {
            fout << endl <<i.attribute->code << endl
                << i.company->code << endl
                << i.value << endl
                << i.date.day << endl
                << i.date.month << endl
                << i.date.year;

        }
    }
    else {
        cout << "Ошибка создания/открытия файла dynamics.txt" << endl;
        result = false;
    }
    return result;
}
void save_in_file(List<Account>& list) {
    std::ofstream fout("save/account.txt", std::ios_base::out);
    if (fout.is_open()) {
        Account temp;
        for (auto &i : list)
            fout << endl <<i.login << " " << i.isAdmin;
        fout.close();
    }
    else {
        cout << "Ошибка создания/перезаписи файла account.txt" << endl;
    }
}

bool load_from_file(List<Attribute> &listAttribute) {
    bool result = true;
    std::ifstream fin("save/attribute.txt", std::ios_base::in);
    if (fin.is_open()) {
        Attribute tstats;
        fin.ignore(1);
        while (!fin.eof()) {
            fin >> tstats.code >> tstats.name
                >> tstats.importance >> tstats.unit;
            if (find(listAttribute, tstats.code) == nullptr) {
                listAttribute.push_back(tstats);
            }
        }
    }
    else {
        cout << "Ошибка открытия файла attribute.txt" << endl;
        result = false;
    }
    return result;
}
bool load_from_file(List<Company> &listCompany) {
    bool result = true;
    std::ifstream fin("save/companies.txt", std::ios_base::in);
    if (fin.is_open()) {
        Company tcompany;
        fin.ignore(1);
        while (!fin.eof()) {
            fin >> tcompany.code >> tcompany.name
                >> tcompany.bank_details >> tcompany.contact_person >> tcompany.telephone;
            if (find(listCompany, tcompany.code) == nullptr) {
                listCompany.push_back(tcompany);
            }
        }
    }
    else {
        cout << "Ошибка открытия файла companies.txt" << endl;
        result = false;
    }
    return result;
}
bool load_from_file(List<Attribute> &listAttribute, List<Company> &listCompany,
    List<AttributeValue> &listAttributeValue) {
    bool result;
    std::ifstream fin("save/dynamics.txt", std::ios_base::in);
    if (fin.is_open()) {
        AttributeValue tstatsd;
        fin.ignore(1);
        us_int a[6];
        while (!fin.eof()) {
            fin >> a[0] >> a[1] >> a[2]
                >> a[3] >> a[4] >> a[5];
                tstatsd.attribute = find(listAttribute, a[0]);
                tstatsd.company = find(listCompany, a[1]);
                tstatsd.value = a[2];
                tstatsd.date.day = a[3];
                tstatsd.date.month = a[4];
                tstatsd.date.year = a[5];

                if (tstatsd.attribute == nullptr) {
                    cout << "Не найден показатель с кодом" << a[0] << endl;
                    result = false;
                }
                else if (tstatsd.company == nullptr) {
                    cout << "Не найдена компания с кодом " << a[1] << endl;
                    result = false;
                }
                else {
                    listAttributeValue.push_back(tstatsd);
                    result = true;
                }
        }
    }
    else {
        cout << "Ошибка открытия файла dynamics.txt" << endl;
        result = false;
    }
    return result;
}
bool load_from_file(List<Account> &list) {
    bool result = true;
    std::ifstream fin("save/account.txt", std::ios_base::in);
    if (fin.is_open()) {
        Account temp;
        fin.ignore(1);
        while (!fin.eof()) {
            fin >> temp.login >> temp.isAdmin;
            list.push_back(temp);
        }
    }
    else {
        cout << "Ошибка открытия файла account.txt" << endl;
        result = false;
    }
    return result;
}

void admin_file(List<Attribute> &listAttribute,
    List<Company> &listCompany,
    List<AttributeValue> &listAttributeValue) {
    int menu = file_menu();
    system("cls");
    switch (menu) {
    case '1':
        if (!save_in_file(listAttribute) ||
            !save_in_file(listCompany) ||
            !save_in_file(listAttributeValue)) {
            system("pause");
            system("cls");
        }
        else {
            cout << "Информация успешно сохранена в файл" << endl;
        }
        break;
    case '2':
        if (!load_from_file(listAttribute) ||
            !load_from_file(listCompany) ||
            !load_from_file(listAttribute, listCompany, listAttributeValue)) {
            system("pause");
            system("cls");
        }
        else {
            cout << "Информация успешно загружена из файла" << endl;
        }
        break;
    case 27:
    case '0':
        break;
    default:
        cout << "Не верный ввод" << endl;
        break;
    }
}

void filter_setting(FilterAttribute &aFilter) {
	bool flag = true;
	char menu;
	do {
		menu = filter_attribute_menu();
		system("cls");
		switch (menu){
		case '1':
			setFilterAttrCode(aFilter);
			system("cls");
			break;
		case '2':
			setFilterAttrName(aFilter);
			system("cls");
			break;
		case '3':
			setFilterAttrImportance(aFilter);
			system("cls");
			break;
		case '4':
			setFilterAttrUnit(aFilter);
			system("cls");
			break;
		case '5':
			aFilter.clear();
			break;
		case 27:
		case '0':
			flag = false;
			break;
		default:
			cout << "Не верный ввод: " << menu << endl;
			break;
		}
	} while (flag);
}

void filter_setting(FilterCompany &cFilter) {
	bool flag = true;
	char menu;
	do {
		menu = filter_company_menu();
		system("cls");
		switch (menu) {
		case '1':
			setFilterCompanyCode(cFilter);
			system("cls");
			break;
		case '2':
			setFilterCompanyName(cFilter);
			system("cls");
			break;
		case '3':
			setFilterCompanyBankDetaild(cFilter);
			system("cls");
			break;
		case '4':
			setFilterCompanyContactPerson(cFilter);
			system("cls");
			break;
		case '5':
			cFilter.clear();
			break;
		case 27:
		case '0':
			flag = false;
			break;
		default:
			cout << "Не верный ввод: " << menu << endl;
			break;
		}
	} while (flag);
}

void filter_setting(FilterValue &vFilter) {
	bool flag = true;
	char menu;
	do {
		menu = filter_value_menu();
		system("cls");
		switch (menu) {
		case '1':
			setFilterValueAttrCode(vFilter);
			system("cls");
			break;
		case '2':
			setFilterValueAttrName(vFilter);
			system("cls");
			break;
		case '3':
			setFilterValueAttrImportance(vFilter);
			system("cls");
			break;
		case '4':
			setFilterValueCompanyCode(vFilter);
			system("cls");
			break;
		case '5':
			setFilterValueCompanyName(vFilter);
			system("cls");
			break;
		case '6':
			setFilterValueDate(vFilter);
			system("cls");
			break;
		case '7':
			vFilter.clear();
			break;
		case 27:
		case '0':
			flag = false;
			break;
		default:
			cout << "Не верный ввод: " << menu << endl;
			break;
		}
	} while (flag);
}

template <typename T, typename G>
void tOut(List<T> &listAttribute, G &aFilter) {
	bool flag = true;
	List<T*> resAFilter;
	char menu;
	do {
		table(listAttribute, aFilter, resAFilter);
		menu = tOut_menu();
		system("cls");
		switch (menu){
		case '1':
			print(resAFilter);
			system("pause");
			system("cls");
			break;
		case '2':
			aFilter.clear();
			break;
		case '3':
			filter_setting(aFilter);
			system("cls");
			break;
		case 27:
		case '0':
		default:
			cout << "Неверный ввод: " << menu << endl;
			break;
		}
	} while (flag);
}

void tablePrint(List<Attribute> &listAttribute,
    List<Company> &listCompany,
    List<AttributeValue> &listAttributeValue,
    FilterAttribute &aFilter, FilterCompany &cFilter,
    FilterValue &vFilter) {
    int menu = tableOut_menu();
    system("cls");
    switch (menu) {
    case '1':
        tOut(listAttribute, aFilter);
        break;
    case '2':
        tOut(listCompany, cFilter);
        break;
    case '3':
        tOut(listAttributeValue, vFilter);
        break;
    case 27:
    case '0':
        break;
    default:
        cout << "Неверный ввод: " << menu << endl;
        break;
    }
}

void ManageAccount() {
    List<Account> list;
    load_from_file(list);
    std::string login;
    bool flag = true;
    do {
        cout << "Всего аккаунтов: " << list.size() << endl;
        switch (account_menu()) {
        case '1':
            cout << "+----------------------+-------+" << endl;
            for (auto &i : list)
                cout << "| " << std::left << std::setw(20) << i.login << " | " << std::left << std::setw(5) << (i.isAdmin ? "admin" : "user") << " |" << endl;
            cout << "+----------------------+-------+" << endl << endl;
            break;
        case '2':
            cout << "+----------------------+" << endl;
            for (auto &i : list)
                if (i.isAdmin)
                    cout << "| " << std::left << std::setw(20) << i.login << " |" << endl;
            cout << "+----------------------+" << endl << endl;
            break;
        case '3':
            cout << "+----------------------+" << endl;
            for (auto &i : list)
                if (!i.isAdmin)
                    cout << "| " << std::left << std::setw(20) << i.login << " |" << endl;
            cout << "+----------------------+" << endl << endl;
            break;
        case '4':
            cout << "Введите логин" << endl;
            std::cin >> login;
            cout << "+----------------------+-------+" << endl;
            for (auto &i : list)
                if (i.login == login)
                    cout << "| " << std::left << std::setw(20) << i.login << " | " << std::left << std::setw(5) << (i.isAdmin ? "admin" : "user") << " |" << endl;
            cout << "+----------------------+-------+" << endl << endl;
            break;
        case '5':
            cout << "Введите логин" << endl;
            std::cin >> login;
            break;
        case '6':
            registration(true, list);
            system("cls");
            break;
        case '7':
            registration(false, list);
            system("cls");
            break;
        case 27:
        case '0':
            flag = false;
            break;
        default:
            system("cls");
            cout << "Введены не верные данные" << endl;
            break;
        }
    } while (flag);
}

void edit_input(Company &val) {

}

void edit_input(Attribute &val) {

}

template <typename T, typename G>
void edit(List<T> &listAttribute, G &aFilter) {
    List<T*> listFilter;
    table(listAttribute, aFilter, listFilter);
    cout << "Вы выбрали редактирование показателей" << endl;
	char menu = 'k';
	us_int t;
	system("cls");
	T* temp;
	switch (menu) {
	case '1':
		filter_setting(aFilter);
		break;
	case '2':
		print(listFilter);
	case '3':
		cout << "Введите номер для редактирования" << endl;
		t = security_int();
		temp = find(listFilter, t);
		if (temp != nullptr) {
			edit_input(*temp);
		}
		else {
			cout << "Ошибка" << endl;
		}
		break;
	case 27:
	case '0':
		break;
	default:
		cout << "Неверный ввод: " << menu << endl;
	}
}

void edit(List<Attribute> &listAttribute, FilterValue &vFilter,
	List<Company> &listCompany, List<AttributeValue> &listValue) {
	List<AttributeValue*> listFilter;
	table(listValue, vFilter, listFilter);
	cout << "Вы выбрали редактирование показателей" << endl;
	char menu = 'k';
	us_int t;
	system("cls");
	AttributeValue* temp;
	Attribute* atr;
	Company* cmp;
	switch (menu) {
	case '1':
		filter_setting(vFilter);
		break;
	case '2':
		print(listFilter);
	case '3':
		cout << "Введите номер для редактирования" << endl;
		t = security_int();
		temp = find(listFilter, t);
		if (!temp) {
			switch (edit_attribute_value_menu()) {
			case 1:
				cout << "Введите код показателя" << endl;
				atr = find(listAttribute, t);
				if (!atr) {
					temp->attribute = atr;
				}
				else {
					cout << "Неверный ввод: " << t << endl;
				}
				break;
			case 2:
				cout << "Введите код компании" << endl;
				cmp = find(listCompany, t);
				if (!cmp) {
					temp->company = cmp;
				}
				else {
					cout << "Неверный ввод: " << t << endl;
				}
				break;
			case 3:
				cout << "Введите значение показателя" << endl;
				break;
			case 4:
				cout << "Введите дату" << endl;
				cout << "День: ";
				temp->date.day = security_int();
				cout << "Месяц: ";
				temp->date.month = security_int();
				cout << "Год: ";
				temp->date.year = security_int();
				break;
			case 5:
				cout << "Введите значение показателя" << endl;
				temp->value = security_int();
				break;
			}
		}
		else {
			cout << "Неверный ввод: " << t << endl;
		}
		break;
	case 27:
	case '0':
		break;
	default:
		cout << "Неверный ввод: " << menu << endl;
	}
}

void edit_data(List<Attribute> &listAttribute,
    List<Company> &listCompany,
    List<AttributeValue> &listAttributeValue,
    FilterAttribute &aFilter, FilterCompany &cFilter,
    FilterValue &vFilter) {
    int menu = edit_menu();
    system("cls");
    switch (menu) {
    case '1':
        edit(listAttribute, aFilter);
        break;
    case '2':
        edit(listCompany, cFilter);
        break;
    case '3':
        edit(listAttribute, vFilter, listCompany, listAttributeValue);
        break;
    case 27:
    case '0':
        break;
    default:
        system("cls");
        cout << "Не верный ввод" << endl;
        break;
    }
}

template <typename T, typename G>
void del(List<T> &list, G &aFilter) {
	char ch;
    List<T*> listFilter;
    table(list, aFilter, listFilter);
    cout << "Вы выбрали удаление показателей" << endl;
    char menu = del_menu();
    system("cls");
    switch (menu) {
    case '1':
		filter_setting(aFilter);
        break;
    case '2':
        print(listFilter);
        system("pause");
        system("cls");
		break;
    case '3':
        cout << "Данные будут удалены " << endl
            << "Продолжить?(1/0)" << endl;
        ch = _getch();
        if (ch == '1') {
			list.del(aFilter);
        }
        else if((ch != '0') && (ch != 27)){
            cout << "Неверный ввод: " << ch << endl;
        }
        break;
    case '4':
        cout << "Данные будут удалены " << endl
            << "Продолжить?(1/0)" << endl;
        ch = _getch();
        if (ch == '1') {
            list.clear();
        }
        else if ((ch != '0') && (ch != 27)) {
            cout << "Неверный ввод: " << ch << endl;
        }
        break;
    case 27:
    case '0':
        break;
    default:
        cout << "Неверный ввод: " << menu << endl;
    }
}

void check(List<AttributeValue> &val) {
	for (auto i = val.begin(); i != val.end(); ++i) {
		if ((*i).attribute || (*i).company)
			i.clear();
	}
}

void delete_data(List<Attribute> &listAttribute,
    List<Company> &listCompany,
    List<AttributeValue> &listAttributeValue,
    FilterAttribute &aFilter, FilterCompany &cFilter,
    FilterValue &vFilter) {
    int menu = delete_menu();
    system("cls");
    switch (menu) {
    case '1':
        del(listAttribute, aFilter);
		check(listAttributeValue);
		break;
    case '2':
        del(listCompany, cFilter);
		check(listAttributeValue);
        break;
    case '3':
        del(listAttributeValue, vFilter);
        break;
    case 27:
    case '0':
        break;
    default:
        system("cls");
        cout << "Не верный ввод" << endl;
        break;
    }
}

void admin() {
    cout << "Вы успешно авторизировались с правами администратора" << endl;

    List<Attribute> listAttribute;
    List<Company> listCompany;
    List<AttributeValue> listAttributeValue;

    FilterAttribute aFilter;
    FilterCompany cFilter;
    FilterValue vFilter;

    bool flag = true;
    system("cls");
    do {
        int menu = admin_menu();
        system("cls");
        switch (menu) {
        case '1':
            admin_file(listAttribute, listCompany, listAttributeValue);
            break;
        case '2':
            enter_data(listAttribute, listCompany, listAttributeValue);
            break;
        case '3':
            tablePrint(listAttribute, listCompany, listAttributeValue,
                aFilter, cFilter, vFilter);
            break;
        case '4':
            edit_data(listAttribute, listCompany, listAttributeValue,
                aFilter, cFilter, vFilter);
            break;
        case '5':
            delete_data(listAttribute, listCompany, listAttributeValue,
                aFilter, cFilter, vFilter);
            break;
        case '6':
            ManageAccount();
            break;
        case 27:
        case '0':
            flag = false;
            system("cls");
            break;
        default:
            cout << "Введены не вырные данные" << endl;
        }
    } while (flag);
}

void user() {
    cout << "Вы успешно авторизировались с правами пользователя" << endl;

    List<Attribute> listAttribute;
    List<Company> listCompany;
    List<AttributeValue> listAttributeValue;

    FilterAttribute aFilter;
    FilterCompany cFilter;
    FilterValue vFilter;

    bool flag = true;
    int menu;
    do {
        menu = user_menu();
        system("cls");
        switch (menu) {
        case '1':
            if (!load_from_file(listAttribute) ||
                !load_from_file(listCompany) ||
                !load_from_file(listAttribute, listCompany, listAttributeValue)) {
                system("pause");
                system("cls");
            }
            else {
                cout << "Информация успешно загружена из файла" << endl;
            }
            break;
        case '2':
            tablePrint(listAttribute, listCompany, listAttributeValue,
                aFilter, cFilter, vFilter);
            break;
        case '3':
            break;
        case 27:
        case '0':
            flag = false;
            break;
        default:
            cout << "Введены не вырные данные" << endl;
            system("pause");
            break;
        }
        system("cls");
    } while (flag);
}