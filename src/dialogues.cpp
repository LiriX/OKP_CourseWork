﻿#include "dialogues.h"
#include <conio.h>

using std::cout;
using std::cin;
using std::endl;

int security_int() {
    std::string temp;
    std::getline(cin, temp);
    return atoi(temp.c_str());
}

std::string enter_string(int x, char z) {
    char ch;
    int value = 0;
    char str[51];
    do{
    ch = _getch();
    switch (ch)
    {
    case '\b':
        if (value) {
            cout << '\b' << '\0' << '\b';
            str[--value] = '\0';
        }
        break;
    case 13:
        cout << '\n';
        str[value] = '\0';
        break;
    case 27:
        for (int i = 0; i < value; i++)
            cout << '\b' << '\0' << '\b';
        value = 0;
        str[value] = '\0';
        break;
    default:
        if (value < 50) {
            str[value++] = ch;
            cout << (z ? z : ch);
        }
        break;
    }
} while (ch != 13);
    return std::string(str);
}

char first_menu() {
    cout << "Первый запуск программы" << endl
        << "1. Создать аккаунт администратора" << endl
        << "0. Выход" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char account_menu()
{
    cout << "1. Просмотреть список всех аккаунтов" << endl
        << "2. Просмотреть список администраторов" << endl
        << "3. Просмотреть список пользователей" << endl
        << "4. Поиск аккаунта по логину" << endl
        << "5. Удаление аккаунта по логину" << endl
        << "6. Создать аккаунт администратора" << endl
        << "7. Создать аккаунт пользователя" << endl
        << "0. Выход" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char admin_menu()
{
    cout << "1. Создание/открытие файла с данными" << endl
        << "2. Добавление записи" << endl
        << "3. Просмотр данных в табличной форме" << endl
        << "4. Редактирование информации" << endl
        << "5. Удаление записей" << endl
        << "6. Управление пользователями " << endl
        << "0. Выход в меню 1-го уровня" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char data_menu()
{
    cout << "1. Добавить информацию о показателе" << endl
        << "2. Добавить информацию о компании" << endl
        << "3. Добавить информацию о значении показателя" << endl
        << "0. Выход" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char edit_menu()
{
    cout << "1. Редактирование информации о показателях" << endl
        << "2. Редактирование информации о компаниях" << endl
        << "3. Редактирование информации о значениях показателей" << endl
        << "0. Выход" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char delete_menu() {
    cout << "1. Удаление информации о показателях" << endl
        << "2. Удаление информации о компаниях" << endl
        << "3. Удаление информации о значениях показателей" << endl
        << "0. Выход" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char edit_attribute_value_menu()
{
    cout << "1. Изменить показатель для записи" << endl
        << "2 Изменить компанию для записи" << endl
        << "3. Изменить значение показателя" << endl
		<< "4. Изменить дату" << endl
        << "0. Назад" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char file_menu()
{
    cout << "1. Сохранить в файл" << endl
        << "2. Загрузить из файла" << endl
        << "0. Выход" << endl;
    cout << "Выберите пункт меню" << endl;
    return _getch();
}

char filter_attribute_menu()
{
	cout << "1. Установить фильтр по коду" << endl
		<< "2. Установить фильтр по названию" << endl
		<< "3. Установить фильтр по степени важности" << endl
		<< "4. Установить фильтр по еденице измерения" << endl
		<< "5. Сбросить настройки фильтра" << endl
        << "0. Назад" << endl
        << "Выберите пункт меню" << endl;
    return _getch();
}

char filter_company_menu()
{
    cout << "1. Установить фильтр по коду" << endl
        << "2. Установить фильтр по названию" << endl
        << "3. Установить фильтр по банковским риквизитам" << endl
        << "4. Установить фильтр по контактрону лицу" << endl
		<< "5. Сбросить настройки фильтра" << endl
        << "0. Назад" << endl;
    cout << "Выберите пункт меню" << endl;
    return _getch();
}

char filter_value_menu()
{
    cout << "1. Код показателя" << endl
        << "2. Название показателя" << endl
        << "3. Cтепень важности показателя" << endl
        << "4. Код компании" << endl
        << "5. Название компании" << endl
        << "6. Дата" << endl
		<< "7. Сбросить настройки фильтра" << endl
        << "0. Назад" << endl;
    cout << "Выберите пункт меню" << endl;
    return _getch();
}

char del_menu()
{
    cout << "1. Настроить фильтр" << endl
        << "2. Просмотр удаляемых данных в табличной форме" << endl
        << "3. Удалить по фильтру" << endl
        << "4. Удалить все записи" << endl
        << "0. Выход" << endl;
    return _getch();
}

char tOut_menu()
{
	cout << "1. Просмотр данных в табличной форме" << endl
		<< "2. Сбросить настройки фильтра" << endl
		<< "3. Настроить фильтр" << endl
		<< "0. Выход" << endl;
	return _getch();
}

char tableOut_menu()
{
	cout << "1. Вывод списка показателей на экран в табличной форме" << endl
		<< "2. Вывод списка компаний на экран в табличной форме" << endl
		<< "3. Вывод списка значений показателей на экран в табличной форме" << endl
		<< "0. Выход" << endl
		<< "Выберите пункт меню" << endl;
    return _getch();
}

char main_menu()
{
    cout << "Меню: " << endl;
    cout << "1. Вход под администратором" << endl;
    cout << "2. Вход под пользователем" << endl;
    cout << "0. Выход" << endl;
    cout << "Выберите пункт меню" << endl;
    return _getch();
}

char user_menu()
{
    cout << "1. Открытие файла с данными" << endl
        << "2. Поиск данных" << endl
        << "3. Дополнительные функции для работы с данными" << endl
        << "0. Выход в меню 1-го уровня" << endl;
    cout << "Выберите пункт меню" << endl;
    return _getch();
}

void check(std::string &val) {
    std::string::size_type length = val.length();
    std::string::size_type i = 0;
    while ((val[i++] != '@') && (i < length));
    if (i != length)
        val.clear();
}

void setFilterAttrCode(FilterAttribute &val) {
    cout << "Введите начальный код показателя или \"@\" для отключения фильтра" << endl;
    val.bCode = security_int();
    cout << "Введите конечный код показателя или \"@\" для отключения фильтра" << endl;
    val.eCode = security_int();
}

void setFilterAttrName(FilterAttribute &val){
    cout << "Введите название показателя или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.name);
    check(val.name);
}

void setFilterAttrImportance(FilterAttribute & val)
{
    cout << "Введите степень важности показателя или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.importance);
    check(val.importance);
}

void setFilterAttrUnit(FilterAttribute & val)
{
    cout << "Введите еденицу измерения показателя или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.unit);
    check(val.unit);
}

void setFilterCompanyCode(FilterCompany &val)
{
    cout << "Введите начальный код компании или \"@\" для отключения фильтра" << endl;
    val.bCode = security_int();
    cout << "Введите конечный код компании или \"@\" для отключения фильтра" << endl;
    val.eCode = security_int();
}

void setFilterCompanyName(FilterCompany &val)
{
    cout << "Введите название компании или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.name);
    check(val.name);
}

void setFilterCompanyBankDetaild(FilterCompany &val)
{
    cout << "Введите банковские риквизиты или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.bank_details);
    check(val.bank_details);
}

void setFilterCompanyContactPerson(FilterCompany &val)
{
    cout << "Введите контактное лицо компании или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.contact_person);
    check(val.contact_person);
}

void setFilterCompanyTelephone(FilterCompany &val)
{
    cout << "Введите контактный номер телефона или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.telephone);
    check(val.telephone);
}

void setFilterValueAttrCode(FilterValue & val)
{
    cout << "Введите начальный код показателя или \"@\" для отключения фильтра" << endl;
    val.attribute.bCode = security_int();
    cout << "Введите конечный код показателя или \"@\" для отключения фильтра" << endl;
    val.attribute.eCode = security_int();
}

void setFilterValueAttrName(FilterValue & val)
{
    cout << "Введите название показателя или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.company.name);
    check(val.company.name);
}

void setFilterValueAttrImportance(FilterValue & val)
{
    cout << "Введите  начальную степень важности показателя или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.attribute.importance);
    check(val.attribute.importance);
}

void setFilterValueAttrUnit(FilterValue & val)
{
    cout << "Введите еденицу измерения показателя или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.attribute.unit);
    check(val.attribute.unit);
}

void setFilterValueCompanyCode(FilterValue & val)
{
    cout << "Введите начальный код компании или \"@\" для отключения фильтра" << endl;
    val.company.bCode = security_int();
    cout << "Введите конечный код компании или \"@\" для отключения фильтра" << endl;
    val.company.eCode = security_int();
}

void setFilterValueCompanyName(FilterValue & val)
{
    cout << "Введите название компании или \"@\" для отключения фильтра" << endl;
    std::getline(cin, val.company.name);
    check(val.company.name);
}

void setFilterValueDate(FilterValue & val)
{
    cout << "Введите начальную дату или \"@\", чтобы отключить фильтр: " << endl
        << "День:  ";
    val.bDate.day = security_int();
    cout << "Месяц: ";
    val.bDate.month = security_int();
    cout << "Год:   ";
    val.bDate.year = security_int();

    cout << "Введите конечную дату или \"@\", чтобы отключить фильтр: " << endl
        << "День: ";
    val.eDate.day = security_int();
    cout << "Месяц: ";
    val.eDate.month = security_int();
    cout << "Год: ";
    val.eDate.year = security_int();
}

void print(List<Attribute*> &list) {
    cout << "Список показателей:" << endl;
    cout << "+-------+----------------------+----------------------+----------------------+" << endl
         << "|  код  |       название       |   степень важности   |   еденица измерения  |" << endl
         << "+-------+----------------------+----------------------+----------------------+" << endl;
    for (auto &i : list) {
        cout << "| " << std::setw(5) << std::right << i->code 
            << " | " << std::setw(20) << std::right << i->name
            << " | " << std::setw(20) << std::right << i->importance
            << " | " << std::setw(20) << std::right << i->unit << " | " << endl;
    }
    cout << "+-------+----------------------+----------------------+----------------------+" << endl;
}

void print(List <Company*> & list) {
    cout << "Список компаний" << endl;
    cout << "+-------+----------------------+----------------------+----------------------+-----------------+" << endl
         << "|  код  |       название       | банковские риквизиты |    контактное лицо   |     телефон     |" << endl
         << "+-------+----------------------+----------------------+----------------------+-----------------+" << endl;
    for (auto &i : list) {
        cout << "| " << std::setw(5) << std::right << i->code 
            << " | " << std::setw(20) << std::right << i->name
            << " | " << std::setw(20) << std::right << i->bank_details
            << " | " << std::setw(20) << std::right << i->contact_person
            << " | " << std::setw(15) << std::right << i->telephone << " |"<< endl;
    }
    cout << "+-------+----------------------+----------------------+----------------------+-----------------+" << endl;
}

void print(List<AttributeValue*> &list) {
    cout << "Значения показателей" << endl;
    cout << "+-----------------------------+-----------------------------+----------+------------+" << endl;
    cout << "|    Название(код) Компании   |   Название(код) показателя  | Значение |    Дата    |" << endl;
    cout << "+-----------------------------+-----------------------------+----------+------------+" << endl;
    std::string a[4];
    for (auto &i : list) {
        a[0] = i->attribute->name + "(" + std::to_string(i->attribute->code) + ")";
        a[1] = i->company->name + "(" + std::to_string(i->company->code) + ")";
        a[2] = std::to_string(i->value) + " " + i->attribute->unit;
        a[3] = std::to_string((int)i->date.day) + ":"
            + std::to_string((int)i->date.month) + ":"
            + std::to_string(i->date.year);
        cout << "| " << std::setw(27) << a[1] << " | " << std::setw(27) << a[0]
            << " | " << std::setw(8) << a[2] << " | " << std::setw(10) << a[3] << " |" << endl
            << "+-----------------------------+-----------------------------+----------+------------+" << endl;
    }
}

void enter_attribute(List<Attribute> &listStats) {
    cout << "Введите код показателя (положительное число): ";
    Attribute temp;
    temp.code = security_int();
    cout << "Введите название показателя(до 50 символов): "; 
    std::getline(cin, temp.name);
    cout << "Введите степень важности показателя: ";
    std::getline(cin, temp.importance);
    cout << "Введите еденицу измерения: ";
    std::getline(cin, temp.unit);
    if (!find(listStats, temp.code)) {
        listStats.push_back(temp);
        cout << "Показатель \"" << temp.name << "\" добавлен в список" << endl;
    }
    else {
        cout << "Код должен быть уникальным" << endl;
    }
}

void enter_company(List<Company> &listCompany) {
    Company temp;
    cout << "Заполните форму" << endl
        << "Код компании: ";
    temp.code = security_int();
    cout << "Название: ";
    std::getline(cin, temp.name);
    cout << "Банковские риквизиты: ";
    std::getline(cin, temp.bank_details);
    cout << "Контактное лицо: ";
    std::getline(cin, temp.contact_person);
    cout << "Телефон: ";
    std::getline(cin, temp.telephone);
    if (!find(listCompany, temp.code)) {
        listCompany.push_front(temp);
        cout << "Компани \"" << temp.name << "\" добавлена в список" << endl;
    }
    else {
        cout << "Код должен быть уникальным" << endl;
    }
}

void enter_attribute_value(List<Attribute> &listStats,
    List<Company> &listCompany,
    List<AttributeValue> &listDynamics) {
    cout << "Заполните форму" << endl;
    AttributeValue temp;
    cout << "Код показателя: ";
    us_int code_s = security_int();
    temp.attribute = find(listStats, code_s);
    if (temp.attribute != nullptr) {
        cout << "Код компании: ";
        us_int code_c = security_int();
        temp.company = find(listCompany, code_c);
        if (temp.company != nullptr) {
            cout << "Численое значение: " << endl;
            temp.value = security_int();
            cout << "Заполните поля для даты: " << endl;
            cout << "Число: ";
            temp.date.day = security_int();
            cout << "Месяц: ";
            temp.date.month = security_int();
            cout << "Год: ";
            temp.date.year = security_int();
            listDynamics.push_back(temp);
            cout << "Запись о показателе \"" << temp.attribute->name 
                << "\" для компании \"" << temp.company->name 
                << "\" успешно добавлена." << endl;
        }
        else {
            cout << "Компания с таким кодом не найдена в списке" << endl;
        }
    }
    else {
        cout << "Показатель с таким кодом не найдена в списке" << endl;
    }
}