﻿#include "algorithms.h"
#include "dialogues.h"

bool KMPSearch(const std::string &string, const std::string &substring) {
    auto sublength = substring.length();
    auto length = string.length();
    size_t* pi = new size_t[length + sublength + 1];
    std::string res = substring + "@" + string;
    auto reslength = length + sublength + 1;
    size_t i = 1;
    size_t j = 0;
    pi[0] = 0;
    bool result = false;
    while ((i < reslength) && (!result)) {
        if (res[i] == res[j]) {
            pi[i] = 1 + j;
            j = j + 1;
            if (pi[i] == sublength) {
                result = true;
            }
            i = i + 1;
        }
        else if (j == 0) {
            pi[i] = 0;
            i = i + 1;
        }
        else {
            j = pi[j - 1];
        }
    }
    delete[] pi;
    return result;
}

std::string hashL(const std::string &x) {
    std::string result = x;
    auto size = result.size();
    auto t = static_cast<char>(result[0] - 7);
    for (size_t i = 0; i < size - 1; i++) {
        result[i] = static_cast<char>(result[i + 1] - 7);
    }
    result[size - 1] = t;
    return result;
}

bool registration(bool isAdmin, List<Account> &list) {
    std::string login;
    std::string password;
    std::cout << "Регистрация" << std::endl
        << "Введите логин(не используя спец. символы и разделители)" << std::endl;
    std::cin >> login;
    std::cout << "Вы ввели: " << login << std::endl
        << "Введите пароль(не используя спец. символы и разделители)" << std::endl;
    std::cin >> password;
    std::cout << "Вы ввели: " << password << std::endl;
    system("pause");
    std::string folder = (isAdmin ? "admin" : "user");
    Account temp = { hashL(login), isAdmin };
    list.push_back(temp);
    save_in_file(list);
    std::ofstream fout(folder + "/" + login + ".txt");
    if (fout.is_open()) {
        fout << hashL(password);
        return true;
    }
    else {
        return false;
    }
}

bool find(List<Account> &list, const std::string &login, bool isAdmin) {
    for (auto &i : list)
        if ((i.login == login) && (i.isAdmin == isAdmin))
            return true;
    return false;
}

bool auth(const std::string & login,
    const std::string & password, const std::string &&folder)
{
    List<Account> list;
    std::ifstream fin("save/account.txt", std::ios_base::in);
    if (fin.is_open()) {
        Account temp;
        while (!fin.eof()) {
            fin >> temp.login >> temp.isAdmin;
            list.push_back(temp);
        }
    }
    fin.close();
    bool isAdmin = folder == "admin";
    bool check = find(list, hashL(login), isAdmin);
    if (!check) return false;
    fin.open(folder + "/" + login + ".txt", std::ios_base::in);
    if (fin.is_open()) {
        std::string pass;
        fin >> pass;
        if (hashL(password) == pass)
            return true;
    }
    return false;
}

Attribute* find(List<Attribute> &list, us_int code) {
    for (auto &it : list)
        if (it.code == code)
            return &it;
    return nullptr;
}

Company* find(List<Company> &list, us_int code) {
    for (auto &it : list)
        if (it.code == code)
            return &it;
    return nullptr;
}

int count(int x) {
    int res = (x % 10) ? 0 : 1;
    while (x) {
        x = x / 10;
        res++;
    }
    return res;
}

int find(List <Company> & list, FilterCompany &x, List<Company*> &listFilter) {
    listFilter.clear();
    int res = 0;
    for (auto &i : list)
        if (i == x) {
            res++;
            listFilter.push_back(&i);
        }
    return res;
}

int find(List<Attribute> &list, FilterAttribute &x, List<Attribute*> &listFilter) {
    listFilter.clear();
    int res = 0;
    for (auto &i : list)
        if (i == x) {
            res++;
            listFilter.push_back(&i);
        }
    return res;
}

int find(List<AttributeValue> &list, FilterValue &x, List<AttributeValue*> &listFilter) {
    listFilter.clear();
    int res = 0;
    for (auto &i : list)
        if (i == x) {
            res++;
            listFilter.push_back(&i);
        }
    return res;
}
