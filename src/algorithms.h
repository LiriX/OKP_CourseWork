﻿#pragma once
#include "struct.h"
#include <fstream>

bool KMPSearch(const std::string &string, const std::string &substring);

int count(int x);
std::string hashL(const std::string &x);
bool auth(const std::string & login,
    const std::string & password, const std::string &&folder);
bool registration(bool isAdmin,
    List<Account> &list);

Attribute* find(List<Attribute> &list, us_int code);
Company* find(List<Company> &list, us_int code);
int find(List <Company> & list, FilterCompany &x, List<Company*> &listFilter);
int find(List<Attribute> &list, FilterAttribute &x, List<Attribute*> &listFilter);
int find(List<AttributeValue> &list, FilterValue &x, List<AttributeValue*> &listFilter);

template <typename T>
T* find(List<T*> & list, us_int code) {
	if (code > list.size())
		return nullptr;
	us_int i = 1;
	auto it = list.begin();
	for (us_int i = 1; i < code; ++i) {
		++it;
	}
	return *it;
}