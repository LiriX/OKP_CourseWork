#include "struct.h"
#include "algorithms.h"

bool operator==(const Attribute &x, const FilterAttribute &y) {
	return ((x.code >= y.bCode) &&
		((x.code <= y.eCode) || (y.eCode == 0)) &&
		(KMPSearch(x.importance, y.importance) || y.importance.empty()) &&
		(KMPSearch(x.name, y.name) || y.name.empty()) &&
		(KMPSearch(x.unit, y.unit) || y.unit.empty()));
}

bool operator==(const Company &x, const FilterCompany &y) {
	return (x.code >= y.bCode) &&
		((x.code <= y.eCode) || (y.eCode == 0)) &&
		(KMPSearch(x.name, y.name) || y.name.empty()) &&
		(KMPSearch(x.bank_details, y.bank_details) || y.bank_details.empty()) &&
		(KMPSearch(x.contact_person, y.contact_person) || y.contact_person.empty()) &&
		(KMPSearch(x.telephone, y.telephone) || y.telephone.empty());
}

bool operator==(const AttributeValue &x, const FilterValue &y) {
	return (*x.attribute == y.attribute) && (*x.company == y.company) &&
		((x.date >= y.bDate) || (!y.bDate)) &&
		((x.date <= y.eDate) || (!y.eDate)) &&
		((x.value >= y.bValue) || (!y.bValue)) &&
		((x.value <= y.eValue) || (!y.eValue));
}