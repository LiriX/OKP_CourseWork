﻿#include "functions.h"

using std::cout;
using std::cin;
using std::endl;

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    bool f = true;
    std::string login;
    std::string password;
    List<Account> listAccount;
    load_from_file(listAccount);
    char menu;
    if (!listAccount.size()) {
		CreateDirectory("save/", NULL);
		CreateDirectory("admin/", NULL);
		CreateDirectory("user/", NULL);
        bool flag = true;
        do {
            menu = first_menu();
            switch (menu)
            {
            case '1':
                registration(true, listAccount);
                break;
            case 27:
            case '0':
                flag = true;
                break;
            default:
                cout << "Неверный ввод: " << menu << endl;
                break;
            }
        } while (flag);
    }
	
	SetFileAttributesW(L"save/", FILE_ATTRIBUTE_HIDDEN);
	SetFileAttributesW(L"admin/", FILE_ATTRIBUTE_HIDDEN);
	SetFileAttributesW(L"user/", FILE_ATTRIBUTE_HIDDEN);
    
	do {
        menu = main_menu();
        system("cls");
        switch (menu) {
        case '1':
            cout << "Вы выбрали вход под администратором." << endl;
            cout << "Логин и пароль вводятся латинскими буквами," << endl 
                 << "проверьте раскладку клавиатуры." << endl;
            cout << "Введите логин" << endl;
            login = enter_string(20);
            cout << "Введите пароль" << endl;
            password = enter_string(50, '*');
            if ((login[0] != '\0') && (password[0] != '\0') && auth(login, password, "admin")) {
                admin();
            }
            else {
                cout << "Неверный логин или пароль" << endl;
            }
            break;
        case '2':
            cout << "Вы выбрали вход под пользователем." << endl;
            cout << "Введите логин" << endl;
            login = enter_string(20);
            cout << "Введите пароль" << endl;
            password = enter_string(50, '*');
            if ((login[0]!= '\0') && (password[0] != '\0') &&
                (auth(login, password, "user") || auth(login, password, "admin"))) {
                user();
            }
            else {
                cout << "Неверный логин или пароль"<< endl;
            }
            break;
        case 27:
        case '0':
            f = false;
            break;
        default:
            std::cout << "Не верный ввод: " << menu << std::endl;
            break;
        }
    } while (f);
    return 0;
}