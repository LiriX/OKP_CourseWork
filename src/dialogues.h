﻿#pragma once
#include <iostream>
#include <iomanip>
#include "algorithms.h"
#include "functions.h"

int security_int();
std::string enter_string(int x, char z = '\0');
char first_menu();
char account_menu();
char admin_menu(); // Меню второго уровня для администратора
char data_menu(); // Выбор данных
char edit_menu(); // Выбора действия
char edit_attribute_value_menu(); // Выбор поля структуры для редактирования
char file_menu(); // Выбор режима работы с файлом
char filter_attribute_menu(); // Настройка параметров фильтра для списка Attribute
char filter_company_menu(); // Настройка параметров фильтра для списка Company
char filter_value_menu(); // Настройка параметров фильтра для списка AttributeValue
char delete_menu();
char del_menu();
char tableOut_menu(); // Выбор списка
char tOut_menu();
char main_menu(); // Меню первого уровня
char user_menu(); // Меню второго уровня для пользователя

void setFilterAttrCode(FilterAttribute &val); // Установить фильтр для списка Attribute по полю code
void setFilterAttrName(FilterAttribute &val); // Установить фильтр для списка Attribute по полю name
void setFilterAttrImportance(FilterAttribute &val); // Установить фильтр для списка Attribute по полю importance
void setFilterAttrUnit(FilterAttribute &val); // Установить фильтр для списка Attribute по полю unit

void setFilterCompanyCode(FilterCompany &val); // Установить фильтр для списка Company по полю code
void setFilterCompanyName(FilterCompany &val); // Установить фильтр для списка Company по полю name
void setFilterCompanyBankDetaild(FilterCompany &val); // Установить фильтр для списка Company по полю bank_details
void setFilterCompanyContactPerson(FilterCompany &val); // Установить фильтр для списка Company по полю contact_person
void setFilterCompanyTelephone(FilterCompany &val); // Установить фильтр для списка Company по полю telephone

void setFilterValueAttrCode(FilterValue &val); // Установить фильтр для списка AttributeValue по полю attribute.code
void setFilterValueAttrName(FilterValue &val); // Установить фильтр для списка AttributeValue по полю attribute.name
void setFilterValueAttrImportance(FilterValue &val); // Установить фильтр для списка AttributeValue по полю attribute.importanse
void setFilterValueAttrUnit(FilterValue &val); // Установить фильтр для списка AttributeValue по полю attribute.importanse
void setFilterValueCompanyCode(FilterValue &val); // Установить фильтр для списка AttributeValue по полю company.code
void setFilterValueCompanyName(FilterValue &val); // Установить фильтр для списка AttributeValue по полю conpany.name
void setFilterValueDate(FilterValue &val); // Установить фильтр для списка AttributeValue по полю date

void print(List<Attribute*> &list); // Вывод на экран в табличном виде
void print(List <Company*> & list); // Вывод на экран в табличном виде
void print(List<AttributeValue*> &list); // Вывод на экран в табличном виде

void enter_attribute(List<Attribute> &listAttribute); // Ввод полей структуры и включение её в список
void enter_company(List<Company> &listCompany); // Ввод полей структуры и включение её в список
void enter_attribute_value(List<Attribute> &listAttribute,
	List<Company> &listCompany,
	List<AttributeValue> &listAttributeValue); // Ввод полей структуры и включение её в список