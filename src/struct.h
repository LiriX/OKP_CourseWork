﻿#pragma once
#include<string>
#include "List.h"

typedef unsigned short int us_int;

struct Date
{
	us_int day; // День
	us_int month; // Месяц
	us_int year; // Год
				 // Конструктор
	Date(us_int x, us_int y, us_int z)
	{
		day = (char)x;
		month = (char)y;
		year = z;
	}
	Date(): day(0), month(0), year(0){} // Конструктор по умолчанию
										  // Обнуление элементов
	void clear()
	{
		day = 0;
		month = 0;
		year = 0;
	}
	// Оператор меньше либо равно
	bool operator <= (const Date &val) const
	{
		return (year < val.year) ||
			(year == val.year) && ((month < val.month) ||
			(month == val.month) && (day <= val.day));
	}
	// Оператор больше либо равно
	bool operator >= (const Date &val) const
	{
		return (year > val.year) ||
			(year == val.year) && ((month > val.month) ||
			(month == val.month) && (day >= val.day));
	}
	// Оператор не равно
	bool operator != (const Date &val) const{
		return (day != val.day) && (month != val.month) && (year != val.year);
	}
	// Оператор приведения к типу bool
	operator bool() const{
		return day && month && year;
	}
};

struct Attribute // Показатели
{
	us_int code; // Код
	std::string name; // Название
	std::string importance; // Важность
	std::string unit; // Еденица измерения
	Attribute(): code(0), name(), importance(), unit() {} // Конструктор по умолчанию
};

struct Company // Компания
{
	us_int code; // Код
	std::string name; // Название
	std::string bank_details; // Банковские риквизиты
	std::string telephone; // Телефон
	std::string contact_person; // Контактное лицо
	Company() : code(0){} // Конструктор по умолчанию
};

struct AttributeValue // Динамика показателей
{
	Attribute* attribute; // Указатель на stats
	Company* company; // Указатель на company
	Date date; // Дата
	long long int value; // Значение
	AttributeValue() :attribute(nullptr),
		company(nullptr),
		date(Date(0, 0, 0)),
		value(0) {}
	AttributeValue(Attribute* stat, Company* comp, Date dt, long long int x): date(dt) {
		attribute = stat;
		company = comp;
		value = x;
	}
};

struct Account {
	std::string login;
	bool isAdmin;
	bool operator!= (const Account &val) const {
		return (login == val.login) && (isAdmin == val.isAdmin);
	}
};

struct FilterAttribute {
	us_int bCode;
	us_int eCode;
	std::string importance;
	std::string name;
	std::string unit;
	void clear() {
		bCode = 0;
		eCode = 0;
		importance.clear();
		name.clear();
		unit.clear();
	}
	FilterAttribute() : bCode(0), eCode(0),
		importance(), name(), unit(){}
};

struct FilterCompany {
	us_int bCode;
	us_int eCode;
	std::string name;
	std::string bank_details;
	std::string telephone;
	std::string contact_person;
	void clear() {
		bCode = 0;
		eCode = 0;
		name.clear();
		bank_details.clear();
		telephone.clear();
		contact_person.clear();
	}
	FilterCompany() : bCode(0), eCode(0), name(),
		bank_details(), telephone(), contact_person() {}
};

struct FilterValue {
	FilterAttribute attribute;
	FilterCompany company;
	Date bDate;
	Date eDate;
	long long int bValue;
	long long int eValue;
	void clear() {
		attribute.clear();
		company.clear();
		bDate.clear();
		eDate.clear();
		bValue = 0;
		eValue = 0;
	}
	FilterValue() : attribute(), company(),
		bDate(), eDate(), bValue(0), eValue(0) {}
};

bool operator==(const Attribute &x, const FilterAttribute &y);

bool operator==(const Company &x, const FilterCompany &y);

bool operator==(const AttributeValue &x, const FilterValue &y);
