﻿#pragma once
#include "List.h"

template <typename T>
class List {
    struct Node {
        Node();
        explicit Node(T & val);
        explicit Node(Node*);
        ~Node();
        T* value;
        Node * next;
        Node * prev;
    };
    unsigned int size_;
    Node * head;
    Node * tail;
public:
    class iterator;
    List();
    explicit List(T&);
    int push_back(T&);
    int push_back(T&&);
    int push_front(T&);
    void clear();
    unsigned int size();
    ~List();
	template <typename G>
    void del(const G &y);
	void check();
    iterator begin();
    iterator end();
    class iterator: public std::iterator<std::input_iterator_tag, T>
    {
    public:
        iterator(); 
        explicit iterator(Node*);
        T& operator*();
        iterator& operator++();
        iterator& operator--();
        bool operator==(const iterator &val);
        bool operator!=(const iterator &val);
		void clear();
    private:
        Node* nodeptr;
    };
};

template<typename T>
List<T>::iterator::iterator(List<T>::Node * val): nodeptr(val) {}

template<typename T>
List<T>::iterator::iterator(): nodeptr(nullptr) {}

template<typename T>
T &List<T>::iterator::operator*() {
    return *nodeptr->value;
}

template<typename T>
typename List<T>::iterator &List<T>::iterator::operator++() {
    nodeptr = nodeptr->next;
    return *this;
}

template<typename T>
typename List<T>::iterator &List<T>::iterator::operator--() {
    nodeptr = nodeptr->prev;
    return *this;
}

template<typename T>
bool List<T>::iterator::operator==(const List::iterator &val) {
    return nodeptr == val.nodeptr;
}

template<typename T>
bool List<T>::iterator::operator!=(const List::iterator &val) {
    return nodeptr != val.nodeptr;
}

template<typename T>
void List<T>::iterator::clear()
{
	auto temp = nodeptr->value;
	nodeptr->value = nullptr;
	delete temp;
}

template <typename T>
List<T>::Node::Node(){
    value = nullptr;
    next = nullptr;
    prev = nullptr;
}

template <typename T>
List<T>::Node::Node(T &val){
    value = new T;
    *value = val;
    next = nullptr;
    prev = nullptr;
}

template<typename T>
List<T>::Node::Node(List<T>::Node *val) {
    value = nullptr;
    next = nullptr;
    prev = val;
}

template<typename T>
List<T>::Node::~Node()
{
    delete value;
}

template <typename T>
List<T>::List(){
    head = new Node;
    tail = head;
    size_ = 0;
}

template <typename T>
List<T>::List(T &val) {
    head = new Node(val);
    tail = new Node;
    head->next = tail;
    size_ = 0;
}

template <typename T>
int List<T>::push_back(T &val) {
    bool isOne = head == tail;
    auto temp = new List<T>::Node(val);
    auto tmp = tail->prev;
    delete tail;
    if(isOne){
        head = tail = temp;
    } else {
        tail = temp;
    }
    temp->prev = tmp;
    if(tmp != nullptr) tmp->next = temp;
    tail->next = new Node(tail);
    tail = tail->next;
    size_++;
    return size_;
}

template <typename T>
int List<T>::push_back(T &&val) {
    bool isOne = head == tail;
    auto temp = new List<T>::Node(val);
    auto tmp = tail->prev;
    delete tail;
    if (isOne) {
        head = tail = temp;
    }
    else {
        tail = temp;
    }
    temp->prev = tmp;
    if (tmp != nullptr) tmp->next = temp;
    tail->next = new Node(tail);
    tail = tail->next;
    size_++;
    return size_;
}

template <typename T>
int List<T>::push_front(T &val) { //TODO исключительные ситуации и пересмотреть алгоритм
    List<T>::Node* temp = new List<T>::Node(val);
        temp->next = head;
        temp->prev = nullptr;
        head->prev = temp;
        head = temp;
    size_++;
    return size_;
}

template<typename T>
void List<T>::clear() {
    while (head != tail) {
        Node * next = head->next;
        delete head;
        head = next;
    }
    size_ = 0;
}

template <typename T>
unsigned int List<T>::size() {
    return size_;
}

template<typename T>
List<T>::~List(){
    while (head != nullptr) {
        Node * next = head->next;
        delete head;
        head = next;
    }
}

template<typename T>
template<typename G>
void List<T>::del(const G &val)
{
    Node* temp = head;
    Node* tmp;
    while (temp != tail) {
        tmp = temp->next;
        if (*temp->value == val) {
            if (temp == head)
                head = temp->next;
			else
                temp->prev->next = temp->next;
            temp->next->prev = temp->prev;
            delete temp;
            size_++;
        }
        temp = tmp;
    }
}

template<typename T>
void List<T>::check()
{
	Node* temp = head;
	Node* tmp;
	while (temp != tail) {
		tmp = temp->next;
		if (temp->value == nullptr) {
			if (temp == head)
				head = temp->next;
			else
				temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
			delete temp;
			size_++;
		}
		temp = tmp;
	}
}

template<typename T>
typename List<T>::iterator List<T>::begin() {
    return List<T>::iterator(head);
}

template<typename T>
typename List<T>::iterator List<T>::end() {
    return List<T>::iterator(tail);
}